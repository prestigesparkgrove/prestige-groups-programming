# Searxng_beetroot

Disroot theme for Searxng.

## How to use
To use this theme, `git clone` it on your computer.

Create a `beetroot` folder in `/var/www/searx/searx/static/themes` and in `/var/www/searx/searx/templates`.

Then, copy the content of `beetroot_static` in `/var/www/searx/searx/static/themes/beetroot` and the content of `beetroot_template` in `/var/www/searx/searx/templates/beetroot`.

Enable the beetroot them by editing `/var/www/searx/searx/settings.yml` and changing:
```
default_theme : beetroot
```

Restart searx: `service uwsgi restart`


## How to edit/change this theme
All changes need to be done to Simple theme and then compiled. So: 
- You have clone the upstream repo first: `git clone https://github.com/searxng/searxng.git searx`.
- Copy the `beetroot_static/src/less/definitions.less` and the `beetroot_static/src/less/disroot.less` from the beetroot theme repo to the clone (in `static/themes/simple/src/less/`).
- Edit the `static/themes/simple/src/less/definitions.less` and `static/themes/simple/src/less/disroot.less` files copied in the clone with the colors and rules you want.
- Once finished, build the theme: in searx root-src folder, as the searx user, do `make themes.simple`.
- Copy the content of the `static/themes/simple/css` folder obtained in the clone back to this repo (if you didn't change any images, you don't have to copy the `images` folder again). Don't forget to also copy `definitions.less` and `disroot.less`, if you changed those, back to the beetroot repo.

If you want to test in live mode all your changes to the `Simple` theme, do `LIVE_THEME=simple make run` from searx-src folder and edit the `static/themes/simple/src/less/definitions.less` and `static/themes/simple/src/less/disroot.less` files as you wish.

## Screenshots

### Light mode
![Light mode](img/Light01.png)
![Light mode](img/Light02.png)
![Light mode](img/Light03.png)

### Dark mode
![Dark mode](img/Dark01.png)
![Dark mode](img/Dark02.png)
![Dark mode](img/Dark03.png)